using System.Collections;

public interface IFPSWeaponPrimaryFireEmptyHandler
{
	void OnEnterPrimaryFireEmpty();

}
