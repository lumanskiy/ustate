using System.Collections;

public interface IFPSWeaponPrimaryFireHandler
{
	void OnEnterPrimaryFire();

}
