﻿using UnityEngine;
using System;
using System.Collections;

public class FPSWeaponImplementation : MonoBehaviour, IFPSWeaponPrimaryFireHandler, IFPSWeaponPrimaryFireEmptyHandler, IFPSWeaponReloadHandler
{
    public AudioClip fireSound;
    public AudioClip emptySound;
    public AudioClip reloadSound;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R) && !state.GetProperty<bool>(FPSWeaponProperties.firePrimary))
        {
            state.SetProperty<bool>(FPSWeaponProperties.reload, true);
        }

        if(Input.GetMouseButtonDown(0) && !state.GetProperty<bool>(FPSWeaponProperties.reload))
        {
            state.SetProperty<bool>(FPSWeaponProperties.firePrimary, true);
        }
    }

    public StateMachine state
    {
        get
        {
            return GetComponent<StateMachine>();
        }
    }

    public void OnEnterPrimaryFire()
    {
        GetComponent<AudioSource>().PlayOneShot(fireSound);

        this.StartCoroutine(DoPrimaryFireAnimation());
    }

    public void OnEnterReload()
    {
        GetComponent<AudioSource>().PlayOneShot(reloadSound);

        this.StartCoroutine(DoReloadAnimation());
    }

    private IEnumerator DoReloadAnimation()
    {
        // Simulate any charge animation.... 
        yield return new WaitForSeconds(2);

        state.SetProperty<bool>(FPSWeaponProperties.reload, false);
        state.AdjustProperty(FPSWeaponProperties.ammo, 10);
    }
    private IEnumerator DoPrimaryFireAnimation()
    {
        // Simulate any charge animation.... 
        yield return new WaitForSeconds(1);

        state.SetProperty<bool>(FPSWeaponProperties.firePrimary, false);
        state.AdjustProperty(FPSWeaponProperties.ammo, -1);
    }

    public void OnEnterPrimaryFireEmpty()
    {
        GetComponent<AudioSource>().PlayOneShot(emptySound);

        this.StartCoroutine(DoPrimaryFireEmptyAnimation());
    }

    private IEnumerator DoPrimaryFireEmptyAnimation()
    {
        // Simulate any charge animation.... 
        yield return new WaitForSeconds(1);

        state.SetProperty<bool>(FPSWeaponProperties.firePrimary, false);

        Debug.Log("No Ammo!");
    }
}
