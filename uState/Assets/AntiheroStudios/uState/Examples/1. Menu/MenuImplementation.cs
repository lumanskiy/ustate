﻿using UnityEngine;
using System.Collections;

public class MenuImplementation : MonoBehaviour, IMenuModelLoginScreenHandler, IMenuModelMainMenuHandler, IMenuModelOptionsMenuHandler
{
    public GameObject loginLayout;
    public GameObject menuLayout;
    public GameObject optionsLayout;

    #region Callbacks
    public void ShowOptions()
    {
        GetComponent<StateMachine>().SetProperty<bool>(MenuModelProperties.showOptions, true);
    }

    public void HideOptions()
    {
        GetComponent<StateMachine>().SetProperty<bool>(MenuModelProperties.showOptions, false);
    }

    public void DoLogIn()
    {
        GetComponent<StateMachine>().SetProperty<bool>(MenuModelProperties.isLoggedIn, true);
    }

    public void DoLogOut()
    {
        GetComponent<StateMachine>().SetProperty<bool>(MenuModelProperties.isLoggedIn, false);
    }

    #endregion

    public void OnEnterLoginScreen()
    {
        loginLayout.SetActive(true);
    }

    public void OnExitLoginScreen()
    {
        loginLayout.SetActive(false);
    }

    public void OnEnterMainMenu()
    {
        menuLayout.SetActive(true);
    }

    public void OnExitMainMenu()
    {
        menuLayout.SetActive(false);
    }

    public void OnEnterOptionsMenu()
    {
        optionsLayout.SetActive(true);
    }

    public void OnExitOptionsMenu()
    {
        optionsLayout.SetActive(false);
    }
}
