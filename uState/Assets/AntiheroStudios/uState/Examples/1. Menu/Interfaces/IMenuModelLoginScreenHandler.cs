using System.Collections;

public interface IMenuModelLoginScreenHandler
{
	void OnEnterLoginScreen();
	void OnExitLoginScreen();

}
