using System.Collections;

public interface IMenuModelOptionsMenuHandler
{
	void OnEnterOptionsMenu();
	void OnExitOptionsMenu();

}
