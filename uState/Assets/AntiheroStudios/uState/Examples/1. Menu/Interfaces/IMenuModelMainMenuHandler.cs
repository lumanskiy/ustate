using System.Collections;

public interface IMenuModelMainMenuHandler
{
	void OnEnterMainMenu();
	void OnExitMainMenu();

}
