﻿using UnityEngine;
using System.Collections;

public class AgentImplementation : MonoBehaviour, IAgentSearchForEnemyHandler, IAgentAttackHandler
{
    public StateMachine state
    {
        get
        {
            return GetComponent<StateMachine>();
        }
    }

    public void OnSearchForEnemy()
    {
        if (!GetComponent<NavMeshAgent>().hasPath)
        {
            Vector3 randomPosition = new Vector3(transform.position.x + Random.Range(-6, 6), transform.position.y, transform.position.z + Random.Range(-5, 6));

            GetComponent<NavMeshAgent>().SetDestination(randomPosition);
        }
    }

    private void OnTriggerStay(Collider collider)
    {
        if (!state.GetProperty<GameObject>(AgentProperties.target))
        {
            if (collider.GetComponent<Enemy>())
            {
                state.SetProperty<GameObject>(AgentProperties.target, collider.gameObject);
            }
        }
    }

    public void OnAttack()
    {
        if(state.GetProperty<GameObject>(AgentProperties.target) != null)
        {
            if (Vector3.Distance(transform.position, state.GetProperty<GameObject>(AgentProperties.target).transform.position) > 1)
            {
                GetComponent<NavMeshAgent>().SetDestination(state.GetProperty<GameObject>(AgentProperties.target).transform.position);
            }
            else
            {
                GameObject.Destroy(state.GetProperty<GameObject>(AgentProperties.target));
            }
        }
    }
}
