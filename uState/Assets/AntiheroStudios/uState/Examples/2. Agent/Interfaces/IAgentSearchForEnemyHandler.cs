using System.Collections;

public interface IAgentSearchForEnemyHandler
{
	void OnSearchForEnemy();

}
